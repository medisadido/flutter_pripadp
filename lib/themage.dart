import 'package:flutter/material.dart';

import './res/Colors/colors.dart';

final lightTheme = ThemeData(brightness: Brightness.light);
final nightTheme = ThemeData(brightness: Brightness.dark);

ThemeData buildAppTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: _buildColorScheme(base.colorScheme),
    textTheme: _buildShrineTextTheme(base.textTheme),
    buttonTheme: _buildButtonTheme(),
    textButtonTheme: _buildTextButtonTheme(),
    textSelectionTheme: const TextSelectionThemeData(
      selectionColor: kShrineBrown100,
    ),
    canvasColor: kShrineBrown100,
    iconTheme: const IconThemeData(size: 24.0, color: kShrineBrown900),
  );
}

ButtonThemeData _buildButtonTheme() {
  return const ButtonThemeData(
    buttonColor: Colors.deepPurple,
    textTheme: ButtonTextTheme.primary,
  );
}

TextButtonThemeData _buildTextButtonTheme() {
  return const TextButtonThemeData(style: ButtonStyle());
}

ColorScheme _buildColorScheme(ColorScheme base) {
  return base.copyWith(
    primary: kShrineBrown100,
    secondary: kShrineBrown900,
    onPrimary: kShrineBrown900,
    error: kShrineErrorRed,
  );
}

TextTheme _buildShrineTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline4: base.headline4!.copyWith(
          fontWeight: FontWeight.w500,
        ),
        headline5: base.headline5!.copyWith(
          fontWeight: FontWeight.w500,
        ),
        headline6: base.headline6!.copyWith(
          fontSize: 18.0,
          fontWeight: FontWeight.w900,
        ),
        caption:
            base.caption!.copyWith(fontWeight: FontWeight.w400, fontSize: 14.0),
        subtitle1: base.subtitle1!.copyWith(
            fontSize: 14,
            color: kShrineBrown900,
            fontWeight: FontWeight.w400,
            height: 1.8),
        // subtitle2,
        bodyText1: base.bodyText1!
            .copyWith(fontWeight: FontWeight.w500, fontSize: 16.0, height: 1.5),
        bodyText2: base.bodyText2!
            .copyWith(fontWeight: FontWeight.w500, fontSize: 17.0, height: 1.5),
        /*
    displayMedium,
    displaySmall,
    headlineLarge,
    headlineMedium,
    headlineSmall,
    titleLarge,
    titleMedium,
    titleSmall,
    bodyLarge,
    bodyMedium,
    bodySmall,
    labelLarge,
    labelMedium,
    labelSmall,

    headline1,
    headline2,
    headline3,

    caption,
    button,
    overline,
    */
      )
      .apply(
        fontFamily: 'Ubuntu',
        displayColor: kShrineBrown900,
        bodyColor: kShrineBrown900,
      );
}

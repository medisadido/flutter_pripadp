import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pripadp/model/pd_arguments.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/states/global.dart';

class PrayOfDayContentScreen extends StatelessWidget {
  static const String routeName = '/pray_day_content';
  const PrayOfDayContentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as PodArguments;

    final _selected = Global.prayers_list
        .firstWhere((element) => element["id"] == args.id, orElse: () {
      return null;
    });
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text("""${_selected["title"]}"""),
            floating: true,
            expandedHeight: 70,
          ),
          SliverList(
            // Use a delegate to build items as they're scrolled on screen.
            delegate: SliverChildBuilderDelegate(
              (context, index) => SectionList(selected: _selected),
              childCount: 1,
            ),
          )
        ],
      ),
    );
  }
}

class SectionWidget extends StatelessWidget {
  const SectionWidget({
    required this.subtitle,
    required this.description,
    Key? key,
  }) : super(key: key);

  final String subtitle, description;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kShrineBrown100,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(subtitle, style: Theme.of(context).textTheme.headline4),
          Container(
            margin: const EdgeInsets.only(top: 5.0),
            padding: const EdgeInsets.all(8.0),
            child:
                Text(description, style: Theme.of(context).textTheme.bodyText2),
          ),
        ],
      ),
    );
  }
}

class SectionList extends StatelessWidget {
  final List<SectionWidget> _sections = [];
  final selected;

  SectionList({Key? key, this.selected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var contents = selected["contents"];
    print(contents.length);

    for (var i = 0; i < contents.length; i++) {
      Map<String, dynamic> jsonraw = contents[i];
      _sections.add(SectionWidget(
          subtitle: jsonraw["subtitle"], description: jsonraw["description"]));
    }
    List<Widget> mainList =
        (_sections.map((item) => Column(children: [item])).toList());
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...mainList,
          Container(
            padding: const EdgeInsets.all(14.0),
            width: MediaQuery.of(context).size.width,
            color: kShrineBrown400,
            child: Text(
              selected["prayer"],
              style: const TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  height: 1.8),
            ),
          )
        ]);
  }
}

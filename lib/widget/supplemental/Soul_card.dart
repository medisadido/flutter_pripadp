import 'dart:math';

import 'package:flutter/material.dart';
import 'package:pripadp/model/soul.dart';

import '../../API/Storer.dart';
import '../../model/pd_arguments.dart';
import '../../res/Colors/colors.dart';
import '../soul_maker_screen.dart';

class SoulCard extends StatefulWidget {
  final double imageAspectRatio;
  Soul soul;
  static const kTextBoxHeight = 65.0;
  final Function? deleteSoul;
  final Function? modifySoul;

  SoulCard({
    this.imageAspectRatio = 33 / 49,
    required this.soul,
    this.deleteSoul,
    this.modifySoul,
    Key? key,
  })  : assert(imageAspectRatio > 0),
        super(key: key);

  @override
  State<SoulCard> createState() => _SoulCardState();
}

class _SoulCardState extends State<SoulCard> {
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final _random = Random();
    final imageWidget = Image.asset(
      widget.soul.assetNames[_random.nextInt(2)],
      fit: BoxFit.cover,
    );

    return Stack(children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          AspectRatio(
            aspectRatio: widget.imageAspectRatio,
            child: imageWidget,
          ),
          SizedBox(
            height: SoulCard.kTextBoxHeight *
                MediaQuery.of(context).textScaleFactor,
            width: 155,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  widget.soul.name,
                  style: theme.textTheme.button,
                  softWrap: false,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 5,
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 4.0),
                Text(
                  widget.soul.relation,
                  style: theme.textTheme.caption,
                ),
              ],
            ),
          ),
        ],
      ),
      Positioned.fill(
          child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  _showDialog(context, widget.soul);
                },
              )))
    ]);
  }

  void _showDialog(context, Soul soul) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        final ThemeData theme = Theme.of(context);

        return AlertDialog(
          insetPadding: const EdgeInsets.all(5),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [
                      RawMaterialButton(
                          elevation: 0,
                          constraints:
                              const BoxConstraints(minWidth: 10, minHeight: 10),
                          fillColor: kShrineBrown100,
                          child: const Icon(Icons.edit_outlined, size: 25.0),
                          padding: const EdgeInsets.all(8.0),
                          shape: const CircleBorder(),
                          onPressed: () async {
                            var podarg =
                                PodArguments(soul.id, "", widget.modifySoul);

                            await Navigator.pushNamed(
                                context, SoulMakerScreen.routeName,
                                arguments: podarg);

                            Storer.getSouls("@souls").then((value) {
                              dynamic contain = value.indexWhere((element) {
                                return element.id == soul.id;
                              });
                              Soul dodo = value[contain];
                              setState(() {
                                widget.soul = dodo;
                                Navigator.pop(context, 1);
                                _showDialog(context, dodo);
                              });
                            });
                          }),
                      RawMaterialButton(
                        onPressed: () async {
                          const actionTextStyle = TextStyle(
                            fontSize: 14,
                            color: kShrineBrown400,
                            leadingDistribution: TextLeadingDistribution.even,
                          );
                          var userableResponse = (await showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: const Text('Supprimer une âme'),
                                  content: const Text(
                                      'Voulez-vous vraiment supprimer cette personne ?'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () => {},
                                      child: const Text('Annuler',
                                          style: actionTextStyle),
                                    ),
                                    TextButton(
                                      onPressed: () async {
                                        widget.deleteSoul!(soul.id);
                                      },
                                      child: const Text(
                                        'Supprimer',
                                        style: actionTextStyle,
                                      ),
                                    ),
                                  ],
                                ),
                              )) ??
                              false;
                        },
                        elevation: 0,
                        fillColor: kShrineBrown100,
                        constraints:
                            const BoxConstraints(minWidth: 10, minHeight: 10),
                        child: const Icon(
                          Icons.delete,
                          size: 25.0,
                        ),
                        padding: const EdgeInsets.all(8.0),
                        shape: const CircleBorder(),
                      ),
                    ],
                  ),
                  TextButton(
                    child: const Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Icon(Icons.clear, color: kShrineBrown900),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    children: [
                      Container(
                          margin: EdgeInsets.zero,
                          padding: const EdgeInsets.all(10),
                          alignment: Alignment.center,
                          width: double.infinity,
                          height: 250,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.contain,
                                  image: AssetImage(
                                      'res/images/toussaint-eternite.png'))),
                          child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Text(soul.name,
                                  style: theme.textTheme.headline4
                                      ?.copyWith(color: Colors.white),
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                  softWrap: true,
                                  maxLines: 999999))),
                      Text(soul.special_pray,
                          style: theme.textTheme.bodyText1,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.start,
                          softWrap: true,
                          maxLines: 999999),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

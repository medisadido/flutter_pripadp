import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pripadp/model/soul.dart';

import 'Soul_card.dart';

class SoulCardColumn {
  factory SoulCardColumn({
    required String type,
    Soul? top,
    Soul? bottom,
    Function? modifySoul,
    Function? deleteSoul,
  }) {
    if (type == 'one') {
      return OneSoulCardColumn(
        modifySoul: modifySoul,
        deleteSoul: deleteSoul,
        top: top ?? const Soul(id: 0, name: "", relation: "", special_pray: ""),
      );
    }
    if (type == 'two') {
      return TwoSoulCardColumn(
          bottom: bottom ??
              const Soul(id: 0, name: "", relation: "", special_pray: ""),
          modifySoul: modifySoul,
          deleteSoul: deleteSoul,
          top: top);
    }

    throw 'Can\'t create $type.';
  }
}

class TwoSoulCardColumn extends StatelessWidget with SoulCardColumn {
  final Function? modifySoul;
  final Function? deleteSoul;

  final Soul bottom;
  final Soul? top;

  TwoSoulCardColumn(
      {required this.bottom,
      this.top,
      this.modifySoul,
      this.deleteSoul,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      const spacerHeight = 44.0;

      double heightOfCards = (constraints.biggest.height - spacerHeight) / 2.0;
      double heightOfImages = heightOfCards - SoulCard.kTextBoxHeight;

      double imageAspectRatio = constraints.biggest.width / heightOfImages;
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsetsDirectional.only(start: 28.0),
            child: top != null
                ? SoulCard(
                    imageAspectRatio: imageAspectRatio,
                    soul: top!,
                    modifySoul: modifySoul,
                    deleteSoul: deleteSoul)
                : SizedBox(
                    height: heightOfCards,
                  ),
          ),
          const SizedBox(height: spacerHeight),
          Padding(
            padding: const EdgeInsetsDirectional.only(end: 28.0),
            child: SoulCard(
                imageAspectRatio: imageAspectRatio,
                soul: bottom,
                deleteSoul: deleteSoul,
                modifySoul: modifySoul),
          ),
        ],
      );
    });
  }
}

class OneSoulCardColumn extends StatelessWidget with SoulCardColumn {
  final Function? modifySoul;
  final Function? deleteSoul;
  final Soul top;

  OneSoulCardColumn(
      {required this.top, Key? key, this.modifySoul, this.deleteSoul})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SoulCard(soul: top, deleteSoul: deleteSoul, modifySoul: modifySoul),
        const SizedBox(
          height: 40.0,
        ),
      ],
    );
  }
}

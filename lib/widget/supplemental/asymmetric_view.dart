import 'package:flutter/material.dart';
import 'package:pripadp/model/soul.dart';

import 'soul_columns.dart';

class AsymmetricView extends StatelessWidget {
  final List<Soul> souls;
  final Function? modifySoul;
  final Function? deleteSoul;

  const AsymmetricView(
      {Key? key, required this.souls, this.modifySoul, this.deleteSoul})
      : super(key: key);

  List<Widget> _buildColumns(BuildContext context) {
    if (souls.isEmpty) {
      return <Container>[];
    }
    return List.generate(_listItemCount(souls.length), (int index) {
      double width = .59 * MediaQuery.of(context).size.width;
      Widget column;
      if (index % 2 == 0) {
        int bottom = _evenCasesIndex(index);
        column = SoulCardColumn(
          type: "two",
          top: souls.length - 1 >= bottom + 1 ? souls[bottom + 1] : null,
          bottom: souls[bottom],
          modifySoul: modifySoul,
          deleteSoul: deleteSoul,
        ) as Widget;
        width += 32.0;
      } else {
        column = SoulCardColumn(
          type: "one",
          top: souls[_oddCasesIndex(index)],
          modifySoul: modifySoul,
          deleteSoul: deleteSoul,
        ) as Widget;
      }
      return SizedBox(
        width: width,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: column,
        ),
      );
    }).toList();
  }

  int _evenCasesIndex(int input) {
    return input ~/ 2 * 3;
  }

  int _oddCasesIndex(int input) {
    assert(input > 0);
    return (input / 2).ceil() * 3 - 1;
  }

  int _listItemCount(int totalItems) {
    if (totalItems % 3 == 0) {
      return totalItems ~/ 3 * 2;
    } else {
      return (totalItems / 3).ceil() * 2 - 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.fromLTRB(0.0, 34.0, 16.0, 44.0),
      children: _buildColumns(context),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/routes/routes.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(context),
          _createDrawerItem(
              icon: Icons.drive_file_rename_outline,
              text: StringLocalizations.of(context).menu_intro,
              onTap: () =>
                  Navigator.pushReplacementNamed(context, Routes.intro)),
          const Divider(
            height: 2.0,
          ),
          _createDrawerItem(
              icon: Icons.event,
              text: StringLocalizations.of(context).menu_days,
              onTap: () => Navigator.pushReplacementNamed(context, Routes.day)),
          _createDrawerItem(
              icon: Icons.person_pin,
              text: StringLocalizations.of(context).menu_dear,
              onTap: () =>
                  Navigator.pushReplacementNamed(context, Routes.souls)),
          const Divider(
            height: 2.0,
          ),
          _createDrawerItem(
              icon: Icons.event,
              text: StringLocalizations.of(context).menu_prays,
              onTap: () =>
                  Navigator.pushReplacementNamed(context, Routes.prays)),
          _createDrawerItem(
              icon: Icons.person_pin,
              text: StringLocalizations.of(context).menu_tips,
              onTap: () =>
                  Navigator.pushReplacementNamed(context, Routes.tips)),
          const Divider(
            height: 2.0,
          ),
          _createDrawerItem(
              icon: Icons.settings,
              text: StringLocalizations.of(context).menu_setting,
              onTap: () =>
                  Navigator.pushReplacementNamed(context, Routes.setting)),
          _createDrawerItem(
              icon: Icons.info_outline,
              text: StringLocalizations.of(context).menu_about,
              onTap: () =>
                  Navigator.pushReplacementNamed(context, Routes.about)),
          ListTile(
            title: const Text('3.4', style: TextStyle(fontSize: 12)),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _createHeader(BuildContext context) {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: const BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fitWidth,
                image: AssetImage('res/images/ic_nav_header.jpg'))),
        child: Stack(children: const <Widget>[]));
  }

  Widget _createDrawerItem(
      {IconData? icon, String? text, GestureTapCallback? onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(text!),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:pripadp/API/Storer.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/res/Strings/arrays.dart';

import '../model/soul.dart';

class SoulMakerScreen extends StatelessWidget {
  static const String routeName = '/soul_maker';

  const SoulMakerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
        appBar: AppBar(title: const Text('Ajouter un être cher')),
        body: SoulForm(arguments: arguments));
  }
}

// Define a custom Form widget.
class SoulForm extends StatefulWidget {
  final arguments;

  const SoulForm({Key? key, this.arguments}) : super(key: key);

  @override
  SoulFormState createState() {
    return SoulFormState();
  }
}

class SoulFormState extends State<SoulForm> {
  String person = "", prayer = "", familyLink = 'âmes chère';
  bool hasSaved = true;
  var isModifying = false;

  final _formKey = GlobalKey<FormState>(),
      prayerController = TextEditingController(text: ""),
      nameController = TextEditingController(text: "");

  @override
  void initState() {
    super.initState();

    if (widget.arguments != null) {
      Storer.getSouls("@souls").then((value) {
        dynamic contain = value.indexWhere((element) {
          return element.id == widget.arguments.id;
        });
        Soul dodo = value[contain];
        setState(() {
          familyLink = dodo.relation;
          prayerController.text = dodo.special_pray;
          nameController.text = person = dodo.name;
        });
      });
    }

    prayerController.addListener(() {
      if (isModifying && prayer != "") {
        hasSaved = false;
      }
      prayer = prayerController.text;
    });
  }

  @override
  void dispose() {
    prayerController.dispose();
    super.dispose();
  }

  Future<bool> _onWillPop() async {
    if (!hasSaved) {
      const actionTextStyle = TextStyle(
        fontSize: 14,
        color: kShrineBrown400,
        leadingDistribution: TextLeadingDistribution.even,
      );
      var userableResponse = (await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Annuler les modifications ?'),
              content: const Text(
                  'Si vous abandonnez cette page vous pouvez perdre les modifications .'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => {Navigator.of(context).pop()},
                  child: const Text('Revenir', style: actionTextStyle),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context, isModifying ? 1 : null);
                  },
                  child: const Text(
                    'Abandonner',
                    style: actionTextStyle,
                  ),
                ),
              ],
            ),
          )) ??
          false;

      return userableResponse == 1;
    } else {
      Navigator.pop(context, 1);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    isModifying = widget.arguments?.id != null;

    List<String> familyLinkArray = ArraysLocalizations.family_link_array;
    // Build a Form widget using the _formKey created above.

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Form(
          key: _formKey,
          child: ListView(
            padding: const EdgeInsets.all(8.0),
            children: <Widget>[
              TextFormField(
                  onChanged: (text) {
                    person = text;
                    hasSaved = false;
                  },
                  controller: nameController,
                  decoration: const InputDecoration(
                    hintText: "Entrez Le Nom du defunt",
                    border: UnderlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Vous devez saisir le nom de la personne !';
                    }
                    return null;
                  }),
              const SizedBox(height: 16),
              DropdownButton<String>(
                value: familyLink,
                icon: const Text(""),
                elevation: 16,
                style: const TextStyle(
                  color: kShrineBrown900,
                ),
                underline: Container(
                  height: 2,
                  color: kShrineBrown900,
                ),
                onChanged: (String? newValue) {
                  setState(() {
                    familyLink = newValue!;
                    hasSaved = false;
                  });
                },
                items: familyLinkArray
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
              const SizedBox(height: 16),
              TextField(
                minLines: 6,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                controller: prayerController,
                decoration: const InputDecoration(
                    hintText: "Saisissez votre prière Spéciale"),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (isModifying) {
                        Soul nesSoul = Soul(
                            id: widget.arguments.id,
                            name: person,
                            relation: familyLink,
                            special_pray: prayer);
                        widget.arguments
                            ?.modifySoul(widget.arguments.id, nesSoul);
                        hasSaved = true;
                      } else {
                        Soul nesSoul = Soul(
                            id: DateTime.now().millisecond,
                            name: person,
                            relation: familyLink,
                            special_pray: prayer);
                        await Storer.addSoul(nesSoul);
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Une personne ajoutée')),
                        );
                        Navigator.pop(context, nesSoul);
                      }
                    }
                  },
                  child: Text(isModifying ? 'Enregistrer' : 'Ajouter'),
                ),
              ),
            ],
          )),
    );
  }
}

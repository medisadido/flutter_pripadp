import 'package:flutter/material.dart';
import 'package:pripadp/model/pd_arguments.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/states/global.dart';

class PrayOfPrayScreen extends StatelessWidget {
  static const String routeName = '/pray_of_pray';

  const PrayOfPrayScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as PodArguments;

    final _selected = Global.prays_list
        .firstWhere((element) => element["id"] == args.id, orElse: () {
      return null;
    });
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(color: kShrineBrown100_2),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              floating: true,
              expandedHeight: 170,
              pinned: true,
              iconTheme: const IconThemeData(color: Colors.white),
              flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fitWidth,
                      image: AssetImage('res/images/bougie.jpg')),
                ),
              )),
              bottom: PreferredSize(
                preferredSize: const Size.fromHeight(0.0),
                child: Container(
                  padding: const EdgeInsets.all(14.0),
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    _selected["title"],
                    style: const TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        height: 1.8),
                  ),
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) => SectionList(selected: _selected),
                childCount: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SectionList extends StatelessWidget {
  final selected;

  const SectionList({Key? key, this.selected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(14.0),
            width: MediaQuery.of(context).size.width,
            color: kShrineBrown100_2,
            child: Text(
              selected["description"],
              style: const TextStyle(
                  fontSize: 18,
                  color: kShrineBrown900,
                  fontWeight: FontWeight.w600,
                  height: 1.8),
            ),
          ),
        ]);
  }
}

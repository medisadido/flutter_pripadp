import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pripadp/module/settings/theme_switcher.dart';
import 'package:pripadp/res/Strings/jsons.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/widget/pray_of_day_screen.dart';
import 'package:pripadp/widget/pray_of_pray.dart';
import 'package:pripadp/widget/soul_maker_screen.dart';

import 'module/About/about.dart';
import 'module/Prays/pray.dart';
import 'module/Souls/soul.dart';
import 'module/Tips/tip.dart';
import 'module/days/day.dart';
import 'module/settings/setting.dart';
import 'module/introduction/intro.dart';
import 'routes/routes.dart';
import 'themage.dart';
import 'package:provider/provider.dart';

class Pripadp extends StatelessWidget {
  const Pripadp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => ThemeProvider(lightTheme),
        child: MaterialApp(
          title: 'Pripadp',
          onGenerateTitle: (BuildContext context) =>
              StringLocalizations.of(context).action_settings,
          localizationsDelegates: const [
            StringLocalizationsDelegate(),
            ArrayLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('en', 'US'),
            Locale('fr', 'FR'),
          ],
          routes: {
            Routes.intro: (context) => IntroPage(),
            Routes.day: (context) => DayPage(),
            Routes.setting: (context) => SetingPage(),
            Routes.tips: (context) => TipPage(),
            Routes.prays: (context) => PrayPage(),
            Routes.souls: (context) => SoulPage(),
            Routes.about: (context) => const AboutPage(),
            Routes.pd_content: (context) => const PrayOfDayContentScreen(),
            Routes.soul_maker: (context) => const SoulMakerScreen(),
            Routes.pp_content: (context) => const PrayOfPrayScreen(),
          },
          theme: _kShrineTheme,
          home: IntroPage(),
        ));
  }
}

final ThemeData _kShrineTheme = buildAppTheme();

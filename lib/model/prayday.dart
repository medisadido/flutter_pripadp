class PrayDay {
  const PrayDay({
    required this.id,
    required this.number,
    required this.title,
  });

  final int id;
  final int number;
  final String title;

  String get assetName => 'res/images/fleur_1.png';

  String get assetPackage => 'res/images';

  @override
  String toString() => "$title (id=$title)";
}

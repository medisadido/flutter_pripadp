class PraySection {
  const PraySection({
    required this.sub_id,
    required this.subtitle,
    required this.description,
  });

  final int sub_id;
  final int subtitle;
  final String description;

  @override
  String toString() => "$sub_id (id=$subtitle)";
}

import 'dart:convert';

class Soul {
  const Soul({
    required this.id,
    required this.name,
    required this.relation,
    required this.special_pray,
  });

  final int id;
  final String name;
  final String relation;
  final String special_pray;

  List<String> get assetNames =>
      ['res/images/candle.jpg', 'res/images/bougie.jpg'];

  String get assetPackage => 'res/images';

  @override
  String toString() => "$name (id=$id)";

  factory Soul.fromJson(Map<String, dynamic> jsonData) {
    return Soul(
      id: jsonData['id'],
      name: jsonData['name'],
      relation: jsonData['relation'],
      special_pray: jsonData['special_pray'],
    );
  }

  static Map<String, dynamic> toMap(Soul soul) => {
        'id': soul.id,
        'name': soul.name,
        'relation': soul.relation,
        'special_pray': soul.special_pray,
      };

  static String encode(List<Soul> souls) => json.encode(
        souls.map<Map<String, dynamic>>((soul) => Soul.toMap(soul)).toList(),
      );

  static List<Soul> decode(String Souls) =>
      (json.decode(Souls) as List<dynamic>)
          .map<Soul>((item) => Soul.fromJson(item))
          .toList();
}

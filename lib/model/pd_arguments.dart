class PodArguments {
  final int id;
  final String title;
  final Function? modifySoul;

  const PodArguments(this.id, this.title, this.modifySoul);

  @override
  String toString() => "$id (id=$title)";
}

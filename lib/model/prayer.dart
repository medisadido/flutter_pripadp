class Prayer {
  const Prayer({
    required this.id,
    required this.title,
  });

  final int id;
  final String title;

  String get assetName => 'res/images/fleur_1.png';
  String get assetPackage => 'res/images';

  @override
  String toString() => "$title (id=$title)";
}

import 'package:pripadp/module/Tips/tip.dart';
import 'package:pripadp/widget/pray_of_day_screen.dart';
import 'package:pripadp/widget/pray_of_pray.dart';
import 'package:pripadp/widget/soul_maker_screen.dart';

import '../module/About/about.dart';
import '../module/Prays/pray.dart';
import '../module/Souls/soul.dart';
import '../module/days/day.dart';
import '../module/settings/setting.dart';
import '../module/introduction/intro.dart';

class Routes {
  static const String intro = IntroPage.routeName;
  static const String day = DayPage.routeName;
  static const String setting = SetingPage.routeName;
  static const String tips = TipPage.routeName;
  static const String prays = PrayPage.routeName;
  static const String souls = SoulPage.routeName;
  static const String about = AboutPage.routeName;
  static const String pd_content = PrayOfDayContentScreen.routeName;
  static const String pp_content = PrayOfPrayScreen.routeName;
  static const String soul_maker = SoulMakerScreen.routeName;
}

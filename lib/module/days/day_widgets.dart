import 'dart:math';

import 'package:flutter/material.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/model/pd_arguments.dart';
import 'package:pripadp/model/prayday.dart';
import 'package:pripadp/widget/pray_of_day_screen.dart';

import '../../API/Storer.dart';

class DayCard extends Card {
  final PrayDay prayday;
  final Function dayTaped;
  final int currentDay;

  const DayCard(
      {Key? key,
      required this.prayday,
      required this.dayTaped,
      required this.currentDay})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Stack(children: <Widget>[
      Card(
        clipBehavior: Clip.antiAlias,
        elevation: 3.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 20 / 10,
              child: Image.asset(
                prayday.assetName,
                fit: BoxFit.fitHeight,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, .0, 16.0, 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      prayday.number.toString(),
                      style: const TextStyle(
                          fontSize: 35,
                          color: kShrineBrown400,
                          leadingDistribution: TextLeadingDistribution.even,
                          fontStyle: FontStyle.italic),
                      softWrap: false,
                    ),
                    const SizedBox(height: 2.0),
                    Text(
                      prayday.title,
                      style: theme.textTheme.button,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      Positioned.fill(
          child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () => {dayTaped(prayday)},
                splashColor: kShrineBrown100,
              ))),
      prayday.number == currentDay
          ? FractionallySizedBox(
              widthFactor: 0.08,
              heightFactor: 0.8,
              child: Container(
                color: kShrineBrown100,
                alignment: const Alignment(-1.0,1.05),
                margin: const EdgeInsets.fromLTRB(4, .0, 0.0, 0),
                child:Transform.rotate(
                    angle: pi/4,
                    child: const FractionallySizedBox(
                      widthFactor: 1,
                      heightFactor: .06,
                      child: DecoratedBox(
                        decoration: BoxDecoration(color: Colors.white),
                      ),
                    )

                ),
              ),

            )
          : const SizedBox(),
    ]);
  }
}

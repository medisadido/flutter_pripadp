import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pripadp/model/prayday.dart';
import 'package:pripadp/res/Strings/jsons.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/states/global.dart';
import 'package:pripadp/utils/common.dart';
import 'package:pripadp/widget/drawer.dart';

import '../../API/Storer.dart';
import '../../model/pd_arguments.dart';
import '../../widget/pray_of_day_screen.dart';
import 'day_widgets.dart';

//Make a collection of cards (102)
List<Card> _buildGridCards(
    BuildContext context, Function dayTaped, int currentDay) {
  final ArrayLocalizations _A = ArrayLocalizations.of(context);
  my_print(_A.prayers_of_day);
  Global.prayers_list = jsonDecode(_A.prayers_of_day);
  List<PrayDay> praydays = [];

  for (var i = 0; i < Global.prayers_list.length; i++) {
    Map<String, dynamic> jsonraw = Global.prayers_list[i];

    PrayDay prayday = PrayDay(
      id: jsonraw['id'],
      number: jsonraw['number'],
      title: jsonraw['title'],
    );

    praydays.add(prayday);
  }

  if (praydays.isEmpty) {
    return const <Card>[];
  }

  return praydays.map((prayday) {
    return DayCard(
        prayday: prayday, dayTaped: dayTaped, currentDay: currentDay);
  }).toList();
}

class DayPage extends StatefulWidget {
  static const String routeName = '/intro';

  @override
  State<DayPage> createState() => _DayPageState();
}

class _DayPageState extends State<DayPage> {
  int currentDay = -1;

  @override
  void initState() {
    super.initState();
    Storer.getCurrentDay().then((value) {
      if (value != null) {
        setState(() {
          currentDay = value;
        });
      }
    });
  }

  dayTaped(PrayDay prayday) async {
    Storer.setCurrentDay(prayday.number);
    setState(() {
      currentDay = prayday.number;
    });
    Navigator.pushNamed(
      context,
      PrayOfDayContentScreen.routeName,
      arguments: PodArguments(prayday.id, prayday.title, null),
    );
  }

  @override
  Widget build(BuildContext context) {
    final StringLocalizations _S = StringLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(_S.menu_days),
      ),
      drawer: AppDrawer(),
      //Add a grid view (102)
      body: GridView.count(
        crossAxisCount: 2,
        padding: const EdgeInsets.all(8.0),
        childAspectRatio: 8.0 / 9.0,
        children: _buildGridCards(context, dayTaped, currentDay),
      ),
      resizeToAvoidBottomInset: true,
    );
  }
}

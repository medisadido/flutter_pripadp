import 'package:flutter/material.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/model/pd_arguments.dart';
import 'package:pripadp/model/prayer.dart';
import 'package:pripadp/widget/pray_of_pray.dart';

class PrayTile extends Card {
  final Prayer prayer;

  const PrayTile({Key? key, required this.prayer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Stack(children: <Widget>[
      Card(
        clipBehavior: Clip.antiAlias,
        elevation: 4.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, .0, 16.0, 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      prayer.title,
                      style: theme.textTheme.headline5,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      maxLines: 6,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      Positioned.fill(
          child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    PrayOfPrayScreen.routeName,
                    arguments: PodArguments(
                      prayer.id,
                      prayer.title,
                      null
                    ),
                  );
                },
                splashColor: kShrineBrown100,
              )))
    ]);
  }
}

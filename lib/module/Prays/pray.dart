import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pripadp/model/prayer.dart';
import 'package:pripadp/module/Prays/pray_widgets.dart';
import 'package:pripadp/res/Strings/jsons.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/states/global.dart';

import '/widget/drawer.dart';

class PrayPage extends StatelessWidget {
  static const String routeName = '/prays';

  @override
  Widget build(BuildContext context) {
    final StringLocalizations _S = StringLocalizations.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(_S.menu_prays),
      ),
      drawer: AppDrawer(),
      //Add a grid view (102)
      body: GridView.count(
        crossAxisCount: 1,
        padding: const EdgeInsets.all(16.0),
        childAspectRatio: 12.0 / 9.0,
        children: _buildGridCards(context),
      ),
      resizeToAvoidBottomInset: true,
    );
  }
}

//Make a collection of cards (102)
List<Card> _buildGridCards(BuildContext context) {
  final ArrayLocalizations _A = ArrayLocalizations.of(context);
  Global.prays_list = jsonDecode(_A.praysArray);

  List<Prayer> prayers = [];

  for (var i = 0; i < Global.prays_list.length; i++) {
    Map<String, dynamic> jsonraw = Global.prays_list[i];
    Prayer prayer = Prayer(id: jsonraw['id'], title: jsonraw['title']);
    prayers.add(prayer);
  }

  if (prayers.isEmpty) {
    return const <Card>[];
  }

  return prayers.map((pray) {
    return PrayTile(
      prayer: pray,
    );
  }).toList();
}

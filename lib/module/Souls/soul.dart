import 'package:flutter/material.dart';
import 'package:pripadp/model/pd_arguments.dart';
import 'package:pripadp/model/soul.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/widget/drawer.dart';
import 'package:pripadp/widget/soul_maker_screen.dart';
import 'package:pripadp/widget/supplemental/asymmetric_view.dart';

import '/widget/drawer.dart';
import '../../API/Storer.dart';

class SoulPage extends StatefulWidget {
  static const String routeName = '/souls';

  @override
  State<SoulPage> createState() => _SoulPageState();
}

class _SoulPageState extends State<SoulPage> {
  List<Soul> soulsList = [];

  _SoulPageState() {
    Storer.getSouls("@souls").then((value) => {
          setState(() {
            soulsList = value;
          })
        });
  }

  modifySoul(int id, Soul s) async {
    await Storer.findAndModify(id, s);
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Modifié(e)')),
    );
  }

  deleteSoul(int id) async {
    await Storer.removeSoul(id);
    setState(() {
      soulsList.removeWhere((s) => s.id == id);
    });
    Navigator.pop(context, 1);
    Navigator.pop(context, 1);

    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Une âme supprimée.')),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    final StringLocalizations _S = StringLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(_S.menu_dear),
      ),
      drawer: AppDrawer(),
      body: (soulsList.isEmpty)
          ? Center(
              child: Text(_S.no_saved_dear, style: theme.textTheme.subtitle1),
            )
          : AsymmetricView(
              souls: soulsList, modifySoul: modifySoul, deleteSoul: deleteSoul),
      resizeToAvoidBottomInset: true,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          handleDearMaker(context, null);
        },
        child: const Icon(Icons.add, size: 30),
      ),
    );
  }

  void handleDearMaker(context, int? id) async {
    var podarg;
    if (id != null) {
      podarg = PodArguments(id, "", null);
    }

    final result = await Navigator.pushNamed(
      context,
      SoulMakerScreen.routeName,
      arguments: podarg,
    );

    Storer.getSouls("@souls").then((value) => {
          setState(() {
            soulsList.add(result as Soul);
          })
        });
  }
}

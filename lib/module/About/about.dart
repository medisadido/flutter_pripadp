import 'package:flutter/material.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '/widget/drawer.dart';
import '../../res/Colors/colors.dart';

class AboutPage extends StatelessWidget {
  static const String routeName = '/about';

  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final StringLocalizations _S = StringLocalizations.of(context);

    return Scaffold(
      drawer: const AppDrawer(),
      body: Container(
        decoration: const BoxDecoration(
          color: kShrineBrown900,
          image: DecorationImage(
              image: AssetImage('res/images/bg-about-2.jpg'),
              opacity: .7,
              fit: BoxFit.contain,
              repeat: ImageRepeat.repeat),
        ),
        height: double.infinity,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 50, 10, 10),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical, //.horizontal
            child: Column(
              children: [
                Text(_S.about_content,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(color: Colors.white)),
                RichText(
                  text: TextSpan(
                    text: '\nVous pouvez',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(color: Colors.white),
                    children: const <TextSpan>[
                      TextSpan(
                          text: ' écrire à :',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    String email = 'pripadp@gmail.com';
                    String subject = 'Bonjour';
                    String body =
                        'Decrivez-nous votre expérience avec les prières de votre application .';

                    String emailUrl =
                        "mailto:$email?subject=$subject&body=$body";

                    await launchUrlString(emailUrl);
                  },
                  child: Text("pripadp@gmail.com",
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          ?.copyWith(color: Colors.amber)),
                ),
                Text('Merci ! ',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(color: Colors.white)),
                RawMaterialButton(
                  onPressed: () async {
                    await launchUrlString(
                        "https://play.google.com/store/apps/details?id=com.pripadp.priezpourlesamedupurgatoire111");
                  },
                  fillColor: kShrineBrown100,
                  child: const Text(
                    'Vérifier les mises à jour',
                    style: TextStyle(
                      fontSize: 14,
                      leadingDistribution: TextLeadingDistribution.even,
                    ),
                  ),
                  padding: const EdgeInsets.all(8.0),
                ),
                RawMaterialButton(
                  onPressed: () async {
                    await launchUrlString(
                        "https://www.paypal.com/donate/?hosted_button_id=7DL7GBUTLHVCG");
                  },
                  fillColor: kShrineBrown100,
                  child: const Text(
                    'M\'encourager et faire un don',
                    style: TextStyle(
                      fontSize: 14,
                      leadingDistribution: TextLeadingDistribution.even,
                    ),
                  ),
                  padding: const EdgeInsets.all(8.0),
                ),
              ],
            ),
          ),
        ),
      ),
      resizeToAvoidBottomInset: true,
    );
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/res/Strings/jsons.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/states/global.dart';
import 'package:pripadp/widget/drawer.dart';

class TipPage extends StatelessWidget {
  static const String routeName = '/tips';

  @override
  Widget build(BuildContext context) {
    final StringLocalizations _S = StringLocalizations.of(context);
    return Scaffold(
        appBar: AppBar(
          title: Text(_S.menu_tips),
        ),
        drawer: AppDrawer(),
        body: const MyStatefulWidget());
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    bool _customTileExpanded = false;
    final ArrayLocalizations _A = ArrayLocalizations.of(context);
    Global.tips_list = jsonDecode(_A.tipsArray);
    List<Widget> tipsArray = [];

    for (var i = 0; i < Global.tips_list.length; i++) {
      Map<String, dynamic> jsonraw = Global.tips_list[i];

      const _biggerFont = TextStyle(
        fontSize: 16,
        color: kShrineBrown400,
        leadingDistribution: TextLeadingDistribution.even,
        fontWeight: FontWeight.w900,
      );
      final title = Text(
        jsonraw['title'],
        style: _biggerFont,
      );
      final description = jsonraw['description'];

      tipsArray.add(i.isOdd
          ? Column(
              children: [
                const SizedBox(
                  height: 25,
                ),
                ExpansionTile(
                  title: title,
                  collapsedIconColor: kShrineBrown900,
                  collapsedTextColor: kShrineBrown900,
                  iconColor: kShrineBrown900,
                  textColor: kShrineBrown900,
                  controlAffinity: ListTileControlAffinity.leading,
                  children: <Widget>[Text(description)],
                ),
                const SizedBox(
                  height: 25,
                )
              ],
            )
          : ExpansionTile(
              title: title,
              collapsedIconColor: kShrineBrown900,
              collapsedTextColor: kShrineBrown900,
              iconColor: kShrineBrown900,
              textColor: kShrineBrown900,
              trailing: Icon(
                _customTileExpanded
                    ? Icons.arrow_drop_down_circle
                    : Icons.arrow_forward,
                size: 30,
              ),
              children: <Widget>[Text(description)],
              onExpansionChanged: (bool expanded) {
                setState(() => _customTileExpanded = expanded);
              },
            ));
    }
    return ListView(padding: const EdgeInsets.all(8.0), children: [
      Column(
        children: tipsArray,
      )
    ]);
  }
}

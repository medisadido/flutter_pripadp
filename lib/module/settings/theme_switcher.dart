import 'package:flutter/material.dart';
import 'package:pripadp/themage.dart';

class ThemeProvider extends ChangeNotifier {
  ThemeData _themeData;
  ThemeProvider(this._themeData);

  ThemeData get themeData => _themeData;

  void toggleTheme() {
    if (_themeData == lightTheme) {
      _themeData = nightTheme;
    } else {
      _themeData = lightTheme;
    }
    notifyListeners();
  }
}

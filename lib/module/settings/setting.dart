import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pripadp/model/prayday.dart';
import 'package:pripadp/module/settings/theme_selection_dialog.dart';
import 'package:pripadp/res/Strings/jsons.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/states/global.dart';
import 'package:pripadp/utils/common.dart';
import 'package:pripadp/widget/drawer.dart';

import '../../API/Storer.dart';
import '../../model/pd_arguments.dart';
import '../../widget/pray_of_day_screen.dart';
import 'setting_widgets.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:nb_utils/nb_utils.dart';

//Make a collection of cards (102)
List<Card> _buildGridCards(
    BuildContext context, Function dayTaped, int currentDay) {
  final ArrayLocalizations _A = ArrayLocalizations.of(context);
  my_print(_A.prayers_of_day);
  Global.prayers_list = jsonDecode(_A.prayers_of_day);
  List<PrayDay> praydays = [];

  for (var i = 0; i < Global.prayers_list.length; i++) {
    Map<String, dynamic> jsonraw = Global.prayers_list[i];

    PrayDay prayday = PrayDay(
      id: jsonraw['id'],
      number: jsonraw['number'],
      title: jsonraw['title'],
    );

    praydays.add(prayday);
  }

  if (praydays.isEmpty) {
    return const <Card>[];
  }

  return praydays.map((prayday) {
    return SettingCard(
        prayday: prayday, dayTaped: dayTaped, currentDay: currentDay);
  }).toList();
}

class SetingPage extends StatefulWidget {
  static const String routeName = '/setting';

  @override
  State<SetingPage> createState() => _SetingPageState();
}

class _SetingPageState extends State<SetingPage> {
  int currentDay = -1;

  @override
  void initState() {
    super.initState();
    Storer.getCurrentDay().then((value) {
      if (value != null) {
        setState(() {
          currentDay = value;
        });
      }
    });
  }

  dayTaped(PrayDay prayday) async {
    Storer.setCurrentDay(prayday.number);
    setState(() {
      currentDay = prayday.number;
    });
    Navigator.pushNamed(
      context,
      PrayOfDayContentScreen.routeName,
      arguments: PodArguments(prayday.id, prayday.title, null),
    );
  }

  @override
  Widget build(BuildContext context) {
    final StringLocalizations _S = StringLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(_S.menu_days),
      ),
      drawer: AppDrawer(),
      //Add a grid view (102)
      body: Scaffold(
        body: SettingsList(
          sections: [
            SettingsSection(
              title: Text('Section 1'),
              tiles: [
                SettingsTile(
                  title: Text('Language'),
                  leading: Icon(Icons.language),
                  onPressed: (BuildContext context) {},
                ),
                SettingsTile.switchTile(
                  title: Text('Use System Theme'),
                  leading: Icon(Icons.phone_android),
                  onToggle: (value) async {
                    await showInDialog(
                      context,
                      builder: (context) => ThemeSelectionDaiLog(context),
                      contentPadding: EdgeInsets.zero,
                    );
                    setState(() {});
                  },
                  initialValue: null,
                ),
              ],
            ),
            // SettingsSection(
            //   titlePadding: EdgeInsets.all(20),
            //   title: 'Section 2',
            //   tiles: [
            //     SettingsTile(
            //       title: 'Security',
            //       subtitle: 'Fingerprint',
            //       leading: Icon(Icons.lock),
            //       onPressed: (BuildContext context) {},
            //     ),
            //     SettingsTile.switchTile(
            //       title: Text('Use fingerprint'),
            //       leading: Icon(Icons.fingerprint),
            //       switchValue: true,
            //       onToggle: (value) {},
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
    );
  }
}

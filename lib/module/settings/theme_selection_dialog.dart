import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:pripadp/module/settings/theme_switcher.dart';
import 'package:pripadp/res/Colors/colors.dart';
import 'package:pripadp/themage.dart';

class ThemeSelectionDaiLog extends StatefulWidget {
  final BuildContext buildContext;

  ThemeSelectionDaiLog(this.buildContext);

  @override
  ThemeSelectionDaiLogState createState() => ThemeSelectionDaiLogState();
}

class ThemeSelectionDaiLogState extends State<ThemeSelectionDaiLog> {
  List<String> themeModeList = [];
  int? currentIndex = 0;

  @override
  void initState() {
    super.initState();
    init();
  }

  Future<void> init() async {
    afterBuildCreated(() {
      themeModeList = [
        "Light",
        "Dark",
        "Par defaut",
      ];
    });
    currentIndex = getIntAsync(THEME_MODE_INDEX);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.width(),
      // height: context.height() * 0.37,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: boxDecorationWithRoundedCorners(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8), topRight: Radius.circular(8)),
              backgroundColor: kShrineBrown900,
            ),
            padding: EdgeInsets.only(left: 24, right: 8, bottom: 8, top: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Choisissez un thème",
                    style: boldTextStyle(color: white, size: 16)),
                IconButton(
                  onPressed: () {
                    finish(context);
                  },
                  icon: Icon(Icons.close, size: 22, color: white),
                ),
              ],
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(vertical: 16),
            itemCount: themeModeList.length,
            itemBuilder: (BuildContext context, int index) {
              return RadioListTile(
                value: index,
                activeColor: kShrineBrown900,
                controlAffinity: ListTileControlAffinity.trailing,
                groupValue: currentIndex,
                title: Text(themeModeList[index], style: primaryTextStyle()),
                onChanged: (dynamic val) async {
                  currentIndex = val;

                  ThemeProvider(lightTheme).toggleTheme();
                  setState(() {});
                },
              );
            },
          ),
        ],
      ),
    );
  }
}

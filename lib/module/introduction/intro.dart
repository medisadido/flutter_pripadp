import 'package:flutter/material.dart';
import 'package:pripadp/res/Strings/strings.dart';
import 'package:pripadp/widget/drawer.dart';

import 'intro_widgets.dart';

class IntroPage extends StatelessWidget {
  static const String routeName = '/introduction';

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final StringLocalizations _S = StringLocalizations.of(context);

    final List<String> _events = _S.pray_moments.split("§§§");

    return Scaffold(
        appBar: AppBar(
          title: Text(_S.app_name),
        ),
        drawer: AppDrawer(),
        body: ListView(padding: const EdgeInsets.all(8.0), children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Text(
                  _S.intro_title,
                  style: theme.textTheme.headline5,
                ),
              ),
              AspectRatio(
                aspectRatio: 18 / 11,
                child: Image.asset(
                  "res/images/ic_im2.png",
                  fit: BoxFit.fitWidth,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 10.0),
                  Text(
                    _S.intro_subtitle,
                    style: theme.textTheme.headline6,
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 22.0),
                    child: Text(
                      _S.intro_cus1,
                      style: theme.textTheme.bodyText1,
                    ),
                  ),
                  TextCitation(
                    text: _S.intro_cus2,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 22.0, 8.0, 22.0),
                    child: Text(
                      _S.intro_cus3,
                      style: theme.textTheme.bodyText1,
                    ),
                  ),
                  TextCitation(
                    text: _S.intro_cus4,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 22.0, 8.0, 22.0),
                    child: Text(
                      _S.intro_cus5,
                      style: theme.textTheme.bodyText1,
                    ),
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _events
                          .map((item) => Column(
                                children: [
                                  TextEvent(
                                    text: item,
                                  ),
                                  const SizedBox(height: 10.0),
                                ],
                              ))
                          .toList()),
                ],
              ),
            ],
          ),
        ]));
  }
}

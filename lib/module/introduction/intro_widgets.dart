import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pripadp/res/Colors/colors.dart';

class TextCitation extends StatelessWidget {
  final String text;

  const TextCitation({Key? key, required this.text}) : super(key: key);

  //Make a collection of cards (102)
  @override
  Widget build(BuildContext context) {
    const _biggerFont = TextStyle(
        fontSize: 14,
        color: kShrineBrown400,
        leadingDistribution: TextLeadingDistribution.even,
        fontStyle: FontStyle.italic);
    return Material(
      color: kShrineBrown100,
      elevation: (2.0),
      shape: const BeveledRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(3.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Text(
          text,
          style: _biggerFont,
        ),
      ),
    );
  }
}

class TextEvent extends StatelessWidget {
  final String text;

  const TextEvent({Key? key, required this.text}) : super(key: key);

  //Make a collection of cards (102)
  @override
  Widget build(BuildContext context) {
    const _biggerFont = TextStyle(
      fontSize: 18,
      color: Colors.white,
      fontWeight: FontWeight.w800,
    );
    return Container(
      padding: const EdgeInsets.all(14.0),
      width: MediaQuery.of(context).size.width * 0.9,
      color: kShrineBrown900,
      child: Text(
        text,
        style: _biggerFont,
      ),
    );
  }
}

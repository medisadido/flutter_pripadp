import 'package:shared_preferences/shared_preferences.dart';

import '../model/soul.dart';

class Storer {
  static void setString(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<Soul> addSoul(Soul s) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = await getSouls("@souls");
    value.add(s);
    final String encodedData = Soul.encode(value);
    prefs.setString("@souls", encodedData);
    return s;
  }

  static void setCurrentDay(int day) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("@current_day", day.toString());
  }

  static Future<int?> getCurrentDay() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int value;
    try {
      value = int.parse(prefs.getString("@current_day")??"-1");
    } on Exception catch (_) {
      value = -1;
    }
    return value;
  }

  static Future<Soul> findAndModify(int id, Soul s) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = await getSouls("@souls");

    dynamic contain = value.indexWhere((element) {
      return element.id == id;
    });
    value[contain] = s;
    final String encodedData = Soul.encode(value);
    prefs.setString("@souls", encodedData);

    return s;
  }

  static Future<int> removeSoul(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = await getSouls("@souls");
    value.removeWhere((s) => s.id == id);

    final String encodedData = Soul.encode(value);
    prefs.setString("@souls", encodedData);

    return 1;
  }

  static Future<List<Soul>> getSouls(String key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final String dataString = prefs.getString(key) ?? "";
      return Soul.decode(dataString);
    } catch (e) {
      return [];
    }
  }
}

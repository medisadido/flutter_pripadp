import 'package:flutter/material.dart';

const kShrineBrown50 = Color(0xFFc0825d);
const kShrineBrown100 = Color(0xFFFEDBD0);
const kShrineBrown100_2 = Color(0xFFd8c5bb);
const kShrineBrown300 = Color(0xFFFBB8AC);
const kShrineBrown400 = Color(0xFF8d5533);

const kShrineBrown900 = Color(0xFF5c2b0b);
const kShrineBlueGrey = Color(0xFF607D8B);

const kShrineErrorRed = Color(0xFFC5032B);

const kShrineSurfaceWhite = Color(0xFFFFFBFA);
const kShrineBackgroundWhite = Colors.white;

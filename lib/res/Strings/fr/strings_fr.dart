const strings_fr = {
  "app_name": """priez pour les âmes""",
  "menu_intro": """Introduction""",
  "menu_days": """Quotidien""",
  "menu_tips": """Conseils""",
  "menu_prays": """Prières""",
  "menu_setting": """Paramètre""",
  "menu_about": """A Propos""",
  "navigation_drawer_open": """Open nav""",
  "navigation_drawer_close": """Close nav""",
  "intro_title": """UN MOIS AVEC NOS AMIS : LES ÂMES DU PURGATOIRE""",
  "intro_subtitle":
      """Pourquoi des lectures et des prières pendant un mois ?""",
  "intro_cus1":
      """       On sait bien, dans le monde chrétien, que la prière des vivants est utile aux morts, mais on ne sait pas assez que les suffrages pour les morts sont utiles aux vivants. Oui, la puissance et la gratitude des saintes âmes du purgatoire sont trop peu connues et appréciées, et l’on ne se préoccupe pas assez de recourir à leur intercession. Et pourtant, leur crédit est si grand que si l’expérience de chaque jour n’était là pour en rendre témoignage, à peine pourrait-on le croire.""",
  "intro_cus2":
      """       A la vérité, ces âmes bénies ne peuvent plus gagner de mérites, mais elles ont la faculté de faire valoir leurs mérites antérieurs en notre faveur.""",
  "intro_cus3":
      """       Elles ne peuvent rien obtenir pour elles-mêmes mais les prières qu’elles font pour nous et les souffrances qu’elles endurent touchent vivement le Cœur de Dieu. Et si elles peuvent déjà nous être grandement utiles pendant qu’elles sont dans le lieu de l’expiation, que ne feront-elles pas pour nous lorsqu’elles seront au Ciel ! Comme elles seront reconnaissantes envers leurs bienfaiteurs ! \n\n       Aussi, le plus grand nombre des théologiens, entre autres les saints Liguori, Bellarmin, Suarez… enseignent que l’on peut légitimement et très utilement invoquer les âmes du purgatoire, pour obtenir de Dieu les grâces et les faveurs dont on a besoin, soit pour l’âme, soit pour le corps. Ste Thérèse avait coutume de dire que tout ce qu’elle demandait à Dieu par l’intermédiaire des fidèles trépassés, elle l’obtenait.""",
  "intro_cus4":
      """ « Quand je veux obtenir sûrement une grâce, disait Ste Catherine de Bologne, j’ai recours à ces âmes souffrantes, afin qu’elles présentent ma requête au Seigneur, et la grâce est toujours accordée. » """,
  "intro_cus5":
      """Elle assurait même qu’elle avait reçu par leur entremise bien des faveurs qui ne lui avaient pas été accordées par l’intercession des Saints. \n\n       Il y a notamment certaines faveurs temporelles qui semblent être plus particulièrement réservées à ces âmes : la guérison d’une maladie grave, la préservation d’un danger physique, moral ou spirituel, le mariage et l’entente dans les foyers, trouver un travail… Dieu, sachant combien les hommes attachent de prix à ces biens de second ordre, les a mis, pour ainsi dire, à la disposition des âmes souffrantes, afin de nous inciter par là à leur procurer les plus abondants suffrages. \n\n       Il y a donc tout à gagner pour nous à échanger ainsi nos prières contre celles de nos frères les morts. Admirable don de la Providence et mystère de la Communion des Saints ! En même temps que nous les soulageons par nos prières et que nous les délivrons du purgatoire, ils offrent à Dieu pour nous, leurs mérites acquis sur la terre et nous recevons ainsi, des bénédictions spirituelles et temporelles. \n\n       Que d’avantages, que de consolations de toutes sortes dans la pratique de la charité chrétienne à l’égard des membres de L’Église souffrante ! \n\n       Connaître les âmes du purgatoire, les délivrer, les prier : voilà les trois raisons de ce livret. Qui pourrait affirmer qu’il n’y a personne de sa famille ou de ses proches au purgatoire ? \n \n       Vous pouvez commencer ces lectures et prières Soit :""",
  "pray_moments":
      """début novembre pour le mois qui leur est concerné §§§le 25 novembre pour terminer le plus grand jour de la libération des âmes du purgatoire : NOËL §§§à partir du moment où vous téléchargez cette application . §§§au décès d’une personne aimée §§§lorsque vous vous y sentirez appelé…""",
  "title_activity_scrolling": """ScrollingActivity""",
  "author_name": """Abbé Berlioux""",
  "action_settings": """Settings""",
  "subtitle": """Subtitle""",
  "tips_content_1":
      """ <span> En ce monde, l’homme a trois amis. Quand Dieu l’appelle, à l’heure de la mort pour le juger ; \n\n - l’argent, son ami de prédilection, ne va pas avec lui, il l’abandonne complètement et ne lui sert plus à rien ; \n\n - ses parents, et ses proches l’accompagnent jusqu’à la tombe, lui jettent un peu d’eau bénite au dernier adieu, et retournent tranquillement chez eux ; \n\n - ses bonnes œuvres, le troisième ami, celui dont il s’est peut-être le moins préoccupé durant sa vie. C’est tout le bien qu’il aura accompli pour l’amour de Dieu. \n\n Seules ses bonnes œuvres lui restent fidèles, l’accompagnent devant le Seigneur, le précèdent, parlent en sa faveur et obtiennent pour lui Pardon et Miséricorde. Ames chrétiennes, dans votre testament, n’hésitez pas à effectuer des dons pour des œuvres d’église et vous aurez des amis dévoués qui vous ouvriront les portes du Ciel. \n\n\n A Cologne, deux dominicains étaient réunis par une grande piété et une égale dévotion ></span> aux âmes du purgatoire.Ils vinrent à se promettre que le premier qui mourrait serait secouru par l’autre, de deux Messes par semaine, toute une année. Un jour, l’un des deux, le bienheureux Suzo, apprit que son ami venait de mourir. Il s’empressa de beaucoup prier pour lui, de s’imposer de grandes pénitences, mais il avait totalement oublié les Messes promises… Un matin où Suzo priait à la chapelle, il vit tout à coup son ami lui apparaître ; le cher défunt lui reprocha son infidélité… Suzo cherchait à s’excuser en lui rappelant les nombreuses prières et les bonnes œuvres qu’il avait faites pour lui. Mais le défunt s’écria : « Oh non non ! Cela n’est rien comparé à la Sainte Messe pour éteindre les flammes qui me brûlent ! »… Et il disparut. Suzo, très impressionné, se promit de réparer cet oubli au plus vite. Il alerta plusieurs prêtres pour l’aider à soulager son cher défunt par de nombreuses messes. Au bout de quelques jours de ce charitable secours, le défunt apparut à Suzo environné d’une grande lumière, le visage rayonnant de bonheur et lui dit : « Je vous remercie, mon fidèle ami, de la délivrance que je vous dois. Grâces aux Saintes Messes qui ont été dites pour moi, je suis sorti du purgatoire et je monte au Ciel où je verrai, face à face le Bon Dieu que nous avons adoré si souvent ensemble. » Et il disparut. Grâce à cet évènement et jusqu’à sa mort, le bienheureux Suzo offrit le Saint Sacrifice de la Messe avec une ferveur renouvelée en faveur des âmes du purgatoire. Faisons dire des Messes… Offrons à Dieu le sang de Jésus… St Jean Chrysostome recommandait cette pieuse pratique : « Ayez dans votre maison à une place apparente, une boîte où chacun puisse y déposer l’obole des morts. Employez ces offrandes à faire dire des messes pour vos défunts. » LES PRIÈRES INDULGENCIÉES POUR LES DÉFUNTS Les prières indulgenciées pour  les défunts constituent un des moyens les plus efficaces pour aider les âmes du purgatoire. Les Papes sont revenus sans cesse sur ce point: la doctrine et l\'usage des indulgences sont fondés sur la Révélations. \n\n La doctrine des indulgences est basée sur le pouvoir des clés que possède l’Église; sur la possibilité de réparation que le christ a offerte pour nous sur la communion des saints. L\'indulgence est la remise des peines temporelles du au péchés déjà pardonnés. \n\n Dieu nous a pardonné mais nous devons encore effacer la douleur que nous lui avons faite pour retrouver la lumière pleine et entière et ainsi chasser les ténèbres accumulés. \n\n Les fidèles qui sont dans de bonnes dispositions, reçoivent l\'indulgence partielle ou plénière aux conditions fixées ,grâce au secours de l’Église qui est l\'intendante de la Rédemption et possède le pouvoir de distribuer et d\'appliquer à chacun les mérites du Christ et des Saints. \n\n L’indulgence fait partie des œuvres spirituelles de miséricorde. Nous pouvons gagner des indulgences à condition d’être en état de grâce et d\'avoir l\'intention d\'accomplir les œuvres prescrites. Mais nous devons d\'abord être entièrement détaché du péché et attaché à Dieu de tout notre cœur et avoir pleine et entière confiance en l\'infinie Miséricorde de Dieu. \n\n Avons-nous déjà pensé que ce ne sont pas seulement nos amis défunt et nos connaissances qui attendent notre aide, mains peut-être aussi des personnes dont nous avons été des complices dans le mal et qui sont peut-être punies en partie à cause de nous ? \n\n Récitons donc souvent les brèves oraisons jaculatoires suivantes, pour les âmes du purgatoire. Avec le temps, ces prières deviendront notre priorité spirituelle et les réciteront en telle ou telle circonstance. Nous apprendrons ainsi à vivre en présence et en communion avec les défunts. \n\n Dieu veut que nous fassions fructifier les talents reçus. Nous agissons certainement selon les intentions et les vues du Dieu en utilisant les trésors de grâce de l’Église mis abondamment à notre disposition. Ces œuvres de miséricorde profitent non seulement de nos amis défunts mais à nous-même et à l’Église toute entière. \n\n A chacun des oraison jaculatoires qui souvent est attachée une indulgence. \n\n Depuis la publication de la constitution Apostolique \"Indulgentiarum doctrina\" entée en vigueur les 1er Janvier 1967, l\'indulgence peut être partielle ou plénière selon qu\'elle efface partiellement ou entièrement les peines temporelles du au péché. On indique plus désormais de jours mais seulement indulgence partielle ou plénière. On a voulu ainsi inviter que des fidèles soient tentés dans leurs prières, d\'attribuer plus de poids dans la quantité qu\'a la qualité. un sincère cri du cœur peut avoir plus de valeur auprès de Dieu qu\'une longue prière.""",
  "tips_content_title": """Offrez des messes pour les âmes du purgatoire""",
  "pray_pray_1": """1. Litanies pour les âmes du purgatoire""",
  "pray_pray_2": """2. Credo""",
  "pray_pray_3": """3. De Profundis""",
  "pray_pray_4": """4. Salve Regina""",
  "pray_pray_5": """5. Prière pour les âmes du Purgatoire""",
  "pray_pray_6": """6. oraisons jaculatoires""",
  "pray_pray_7": """7. PRIÈRES A MARIE""",
  "pray_pray_8":
      """8. Le Notre père de sainte Mechtilde pour les âmes du purgatoire""",
  "title_activity_settings": """Settings""",
  "setting_mode_title": """Mode""",
  "setting_police_title": """Police""",
  "setting_apply_button": """Appliquer""",
  "tips_cont_title2": """Conseils Pratiques de L\'Abbé Berlioux, L\'auteur""",
  "tips_content_2":
      """ En terminant les lectures et les prières de ce mois,(qui peut être votre choix) permettez-moi quelques conseils d\'ami, de frère, de prêtre: \n\n Si les âmes du purgatoire sont plus que généreuses à notre égard lorsque nous prions pour elles, êtes-vous certains que vous ne serai pas très vite oublié par les vivants après votre départ de la terre ? \n\n N\'attendez pas votre vieillesse pour y penser et faire enregistrer vo dernières volontés  c)Pourquoi ne pas prévoir un certains montant de ce que vous laisserai en quittant la vie d\'ici-bas pour certaines œuvres d’Église spécialisée dans vos souvenir(Messes et prières) des défunts. \n\n 1. En fait de reconnaissance, comptez beaucoup sur celle des morts, car les mots, disais saint François de Sales, sont toujours reconnaissant de ce qu\'on a fait pour eux dans celles des vivants, surtout s\'ils ne sont pas vos propres enfants. \n\n Vos héritiers vous feront probablement de bonnes funérailles, ils vous donneront peut-être un caveau dans lequel la vanité aura plus large part que la piété filiale et religieuse, mais ils économiseront d\'autant plus à l’Église qu\'ils auront été plus prodigues au cimetière. \n\n Ne savez-vous pas que les hommes, lorsqu\'ils ont perdu quelqu\'un de vue, en ont bientôt perdu le souvenir ? Loin des yeux, loin du cœur !  \n\n Ne savez-vous pas que l\'oubli des mort est presque général dans le monde et que leur mémoire finit souvent avec le son des cloches ?  \n\n Ne savez-vous pas que dans l\'affaire du Salut il ne faut se fier qu\'a soit même ? \n\n Profitez donc de l\'avertissement salutaire que vous donne l\'auteur de l\'imitation. Ne comptez point sur vos amis, ni sur vos propres affaires à gérer; Si maintenant vous ne vous occupez pas de vous-même, qui s\'en occupera, lorsque vous aurez disparu ?  \n\n \"Je vous en prie , disait saint Augustin, avant d\'être accablé par la maladie, occupez-vous de votre testament , réglez les affaires de votre maison; car, si vous attendez d\'être à l\'extrémité, on vous fera faire par menace ou par flatterie ce que vous ne voudrez pas faire\" \n\n\n\n Celui qui se dispose à aller faire un long voyage pour aller habiter un pays lointain, fait des préparatifs en conséquence. Entreprendriez-vous le long voyage du temps à l\'éternité, sans emporter avec vous quelques bonnes œuvres pour vous rendre propice le Seigneur des seigneurs et ouvre la Porte du Ciel ?""",
  "header_nav_img_content_description": """image de tête""",
  "setting_language_title": """Langue""",
  "intro_imgview_content_description":
      """Jesus sauvez les âmes du purgatoires""",
  "sharing_app_text_intent_title": """Merci de partager""",
  "menu_dear": """mes plus chères""",
  "name": """Nom:""",
  "link": """Relation:""",
  "no_saved_dear": """Vous n\'avez enregitré aucun(e) defunt(e) !""",
  "suppress": """Suprimer""",
  "add_favorite": """ajouter aux favoris""",
  "confirm_suppress": """Voulez vous supprimer""",
  "click_on_below_button":
      """cliquez sur le boutton ci-dessous pour ajouter !""",
  "cancel": """fermer""",
  "add": """Ajouter""",
  "modify": """Modifier""",
  "veerify_maj": """<![CDATA[Vérifier les mises à jour >>]]>""",
  "alarm_off": """Rappel quotidien""",
  "notification_title": """Priez pour eux !""",
  "notification_description":
      """Le chemin de la prière est un chemin de vie !""",
  "preference_category_date": """Rappel""",
  "pick_time": """Time""",
  "cancel_reminder": """rappel désactivé !""",
  "next_remin_in": """prochain rapple dans """,
  "information": """info: """,
  "about_content":
      """ Ce programme a été mis en place pour aider à libérer les âmes du purgatoire, ces âmes pauvres qui ne peuvent rien faire pour elles et qui ont désespérément besoin de notre aide. Mais ils peuvent faire beaucoup pour les autres, y compris ceux qui les soulagent ou mieux les relâchent. \n\n Stockage: Toutes les données de l\'application sont stockées sur votre téléphone.\n\n Si vous estimez que ce programme est utile, vous pouvez prendre un peu de temps pour l\'évaluer . vous pouvez nous évaluer avec 5 étoiles et si vous souhaitez également laisser un commentaire, vous serai également le bienvenu pour des suggestions d\'amélioration. \n\n Spéciale dédicace à mon père Sylvain Didier ADIDO , Que ton âme repose en Paix ! \n\n Vous pourrez aussi envoyer des corrections de textes si vous repérez des fautes grammaticales/orthographiques.""",
  "information_local_storage":
      """Toutes les <b>données</b> de l\'application sont stockées localement sur votre téléphone.""",
  "prayers_of_day": """ """
};

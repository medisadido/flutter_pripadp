const arrays_fr = {
  "family_link_array": [
    'âmes chère',
    'Ami(e)',
    'Belle-famille',
    'Épouse',
    'Époux',
    'Neveu/nièce',
    'Cousin(e)',
    'Sœur',
    'Frère',
    'Enfant',
    'Oncle',
    'Tante',
    'Parent',
    'Grands-parents',
    'Arrière-petits-enfants',
    'Arrière-grands-parents',
  ]
};

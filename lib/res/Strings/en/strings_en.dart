const strings_en = {
  "app_name": """ pray for souls """,
  "menu_intro": """ Introduction """,
  "menu_days": """ Days """,
  "menu_tips": """ Tips """,
  "menu_prays": """ Prayers """,
  "menu_setting": """ Parameter """,
  "menu_about": """ About """,
  "author_name": """Abbé Berlioux""",
  "navigation_drawer_open": """ Open nav """,
  "navigation_drawer_close": """ Close nav """,
  "intro_title": """ A MONTH WITH OUR FRIENDS: THE SOULS OF PURGATORY """,
  "intro_subtitle": """ Why readings and prayers for a month? """,
  "intro_text": """fefe""",
  "intro_cus1": """
        \t\t
        We know well in the Christian world that the prayer of the living is useful to the dead, but we do not know
        not enough that the votes for the dead are useful to the living.
        \n\n

        \t\t Yes, the
        power and gratitude of the holy souls in purgatory are too little
        known and appreciated, and too little attention is given to using them
        intercession. And yet their credit is so great that if the experience of each
        day was not there to bear witness, hardly one could believe it.""",
  "intro_cus2": """
         Truth, these blessed souls can no longer earn merits, but they have the
        faculty to assert their previous merits in our favor.""",
  "intro_cus3": """ They don\'t
        can get nothing for themselves but the prayers they do for us
        and the sufferings they endure deeply touch the Heart of God. And if
        they can already be of great use to us while they are in the
        place of atonement, what will they not do for us when they are at
        Heaven! How grateful they will be to their benefactors!
        \n\n

            Also, the greatest number of theologians, among others the Saints Liguori,
        Bellarmin, Suarez … teach that you can legitimately and very usefully
        invoke the souls of purgatory, to obtain graces and favors from God
        which we need, either for the soul or for the body.
        Ste Therese had used to say that whatever she asked of God through the
        faithful dead, she got it.""",
  "intro_cus4":
      """ \"When I surely want to obtain a pardon, said St. Catherine of Bologna, I have recourse to these suffering souls, in order let them present my request to the Lord, and grace is always granted.\"
        \n\n
""",
  "intro_cus5":
      """She even asserted that she had received through them many favors which had not been granted to him by the intercession of the Saints.


            There are in particular certain temporal favors which seem to be more
        particularly reserved for these souls: healing from a serious illness,
        preservation of physical, moral or spiritual danger, marriage and understanding
        in the homes, find a job … God, knowing how much men
        put a price on these second-order goods, put them, so to speak, at the
        disposition of the suffering souls, in order to encourage us there to procure them
        the most abundant votes.
        \n\n

            There is therefore everything to gain for us to exchange our prayers for
        those of our dead brothers. Admirable gift of Providence and mystery of
        the Communion of Saints! At the same time as we relieve them with our
        prayers and we deliver them from purgatory, they offer to God for us,
        their merits acquired on earth and we thus receive blessings
        spiritual and temporal.

        \n\n What advantages, what consolations for all
        kinds in the practice of Christian charity towards the members of the Suffering Church!
        \n\n

        Know the souls in purgatory, deliver them, pray to them:
        these are the three reasons for this booklet. Who could say there is no one
        family or loved ones in purgatory?
        You can start these readings and prayers Either:
        \n

    """,
  "pray_moments": """
        §§§early November for the month concerned
        §§§November 25 to end the biggest day of liberation of souls from purgatory: CHRISTMAS
        §§§from the moment you download this app
        §§§on the death of a loved one
        §§§when you feel called …
        §§§Abbé Berlioux
    """,
  "title_activity_scrolling": """ ScrollingActivity """,
  "action_settings": """ Settings """,
  "subtitle": """ Subtitle """,
  "tips_content_1": """
        <span>
            In this world, man has three friends. When God calls her, at the hour of
        dead to judge him; \n\n
        - money, his favorite friend, doesn\'t go with him, he
        abandons him completely and is no longer useful to him;
        \n\n
        - his parents, and his
        loved ones accompany him to the grave, throw him some holy water at the
        last farewell, and quietly return home;
        \n\n
        - his good works,
        the third friend, the one he probably cared the least about in his lifetime.
        That’s all the good he’s done for the love of God.
        \n\n
        Only his good works remain faithful to him, accompany him before the Lord, precede him,
        speak in his favor and obtain for him Pardon and Mercy.
        Christian souls, in your will, do not hesitate to make donations
        for church works and you will have dedicated friends who will open you up
        the gates of Heaven.
            \n\n \n

            In Cologne, two Dominicans were united by great piety and equal devotion> </span>
        to the souls in purgatory. They came to promise each other that the first to die would be
        rescued by the other, two Masses a week, a whole year. One day, one of the two,
        Blessed Suzo learned that his friend had just died. He hastened to pray a lot
        for him to impose great penance, but he had completely forgotten the Masses
        promised … One morning when Suzo was praying in the chapel, he suddenly saw his friend appear to him;
        the dear deceased reproached him for his infidelity … Suzo tried to apologize by reminding him
        the many prayers and good works he had done for him. But the deceased
        exclaimed: \"Oh no no! This is nothing compared to the Holy Mass to extinguish the
        flames that burn me! \"… And he disappeared. Suzo, very impressed, promised himself to
        repair this oversight as soon as possible. He alerted several priests to help him relieve
        his dear deceased by many masses. After a few days of this charitable
        help, the deceased appeared to Suzo surrounded by a great light, the radiant face
        of happiness and said to him: \"I thank you, my faithful friend, for the deliverance that I owe you.
        Thanks to the Holy Masses that were said for me, I left purgatory and I go up to
        Heaven where I will see, face to face, the good Lord that we have adored so often together.\"
        And he disappeared. Thanks to this event and until his death, Blessed Suzo offered
        the Holy Sacrifice of the Mass with renewed fervor in favor of the souls in purgatory.
        Let us say Masses… Let us offer to God the blood of Jesus… St John Chrysostom recommended
        this pious practice: "Have in your house in an apparent place, a box where each can drop the dead’s object there.
        Use these offerings to have masses said for your deceased. "


        INDULGENTIATED PRAYERS FOR THE DECEASED
        Indulgated prayers for the dead are one of the most effective ways to help
        the souls in purgatory. The Popes have constantly returned to this point: the doctrine and
        the use of indulgences are based on Revelations.
        \n\n

        The doctrine of indulgences is based on the power of keys held by the Church;
        on the possibility of reparation that Christ offered for us on the communion of saints.
        Indulgence is the remission of temporal punishments due to sins already forgiven.
        \n\n

        God has forgiven us but we have yet to erase the pain we have given him
        made to find the full and whole light and thus drive away the accumulated darkness.
        \n\n

        The faithful who are in good mood, receive partial indulgence
        or plenary on the conditions set, thanks to the help of the Church which is the steward
        of Redemption and has the power to distribute and apply to everyone
        the merits of Christ and the Saints.
        \n\n

        Indulgence is one of the spiritual works of mercy.
        We can earn indulgences as long as we are in a state of grace and have
        the intention to accomplish the prescribed works. But first we have to be fully
        detached from sin and attached to God with all our heart and have full confidence
        in the infinite Mercy of God.
        \n\n

        Have we ever thought that it is not only our deceased friends and acquaintances who
        are waiting for our help, perhaps also the people of whom we have been accomplices
        in evil and who are perhaps being punished in part because of us?
        \n\n

        So often recite the following brief ejaculatory prayers,
        for the souls in purgatory. Over time, these prayers will become our priority
        spiritual and will recite them in this or that circumstance. So we will learn
        to live in presence and in communion with the deceased.
        \n\n

        God wants us to use the talents we receive. We certainly act
        according to the intentions and views of God using the treasures of grace of the Church
        made abundantly available to us. These works of mercy not only benefit
        of our deceased friends but to ourselves and to the whole Church.
        \n\n

        To each of the ejaculatory prayers which is often attached an indulgence.
        \n\n

        Since the publication of the Apostolic Constitution \"Indulgentiarum doctrina\" entered into
        effective January 1, 1967, the indulgence may be partial or full depending on whether it
        partially or entirely erases the temporal penalties due to sin. We indicate more
        now days but only partial or full indulgence.
        We wanted to invite the faithful to be tempted in their prayers, to attribute
        more weight in quantity than in quality. a sincere cry from the heart may have more
        of value to God than a long prayer.

    """,
  "tips_content_title": """ Offer masses for souls in purgatory """,
  "pray_pray_1": """ 1. Litanies for the souls in purgatory """,
  "pray_pray_2": """ 2. Credo """,
  "pray_pray_3": """ 3. From Profundis """,
  "pray_pray_4": """ 4. Salve Regina """,
  "pray_pray_5": """ 5. Prayer for the souls in Purgatory """,
  "pray_pray_6": """ 6. ejaculatory prayers """,
  "pray_pray_7": """ 7. PRAYERS TO MARY """,
  "pray_pray_8":
      """ 8. The Our Father of Saint Mechtilde for the souls in purgatory """,
  "title_activity_settings": """ Settings """,
  "setting_mode_title": """ Mode """,
  "setting_police_title": """ Police """,
  "setting_apply_button": """ Apply """,
  "tips_cont_title2": """ Practical Advice from Abbe Berlioux, The Author """,
  "tips_content_2": """
        By finishing this month’s readings and prayers, (which can be your choice)
        allow me some advice from friend, brother, priest: \n\n

          If the souls in purgatory are more than generous to us when we pray
        for them, are you sure that you will not be quickly forgotten by the living after
        your departure from the earth? \n\n

          Don\'t wait until your old age to think about it and
        record your last wishes c) Why not plan a
        some amount of what you will leave when leaving life here below
        for certain works of the Church specialized in your memories (Masses and prayers) of the deceased.
         \n\n

        1. In terms of recognition, count a lot on that of the dead, because the words, said
        Saint François de Sales, are always grateful for what we have done for them
        in those of the living, especially if they are not your own children. \n\n

          Your heirs will probably give you a good funeral, they will give you
        perhaps a vault in which vanity will play a larger part than filial piety
        and religious, but they will save the more the Church the more they have been
        lavish in the cemetery. \n\n

          Do you not know that men, when they lost someone
        of sight, soon lost the memory of it? Out of sight out of mind ! \n\n

          Do you not know that the oblivion of the dead is almost general in the world and that their
        memory often ends with the sound of bells? \n\n

          Do you not know that in the matter of Salvation one should not trust that one is the same? \n\n

        Take advantage of the salutary warning given to you by the author of the imitation.
        Do not rely on your friends or on your own affairs to manage; If now you
        do not take care of yourself, who will take care of it when you are gone? \n\n


        \"Please, said Saint Augustine, before being overwhelmed by illness,take care of your will, settle the affairs of your house; because if you wait until you are at the end, you will be made to do by threat or flattery what you will not want to do Whoever is going to go on a long trip to go and live a distant country, make preparations accordingly. Would you undertake the long journey from time to eternity, without taking with you some good works for make you auspicious the Lord of lords and open the Gate of Heaven?

    """,
  "about_content": """
        This program was set up to help free souls from purgatory,
        these poor souls who can do nothing for them and who desperately need to
        our help. But they can do a lot for others, including those who
        relieve or better release them.
        \n\n\n

        <b>Storage :</b> All app data is stored locally on your phone.


       Special dedication to my father Sylvain Didier ADIDO , May your soul rest in Peace!
        \n\n\n

        If you find this program useful, you can take a little time to review it.
        you can rate us with 5 stars and if you also want to leave a
        comment, you will also be welcome for suggestions for improvement and text corrections/traducing.
        \n\n\n
        You can write to: pripadp@gmail.com
        ☺☺ thank !

    """,
  "header_nav_img_content_description": """ head image """,
  "setting_language_title": """Language""",
  "intro_imgview_content_description": """Jesus save our souls""",
  "sharing_app_text_intent_title": """Thank for sharing""",
  "menu_dear": """My souls""",
  "name": """Name :""",
  "link": """Relation :""",
  "no_saved_dear": """no saved dear.""",
  "suppress": """Delete""",
  "add_favorite": """add to favorite""",
  "confirm_suppress": """confirm deleting""",
  "click_on_below_button": """click on below button to add !""",
  "cancel": """cancel""",
  "add": """add""",
  "modify": """Edit""",
  "veerify_maj": """<![CDATA[Verify update >>]]>""",
  "alarm_off": """Daily reminder""",
  "notification_title": """Pray for them !""",
  "notification_description": """The path of prayer is a path of life!""",
  "preference_category_date": """remind""",
  "pick_time": """Time""",
  "cancel_reminder": """reminder disabled !""",
  "next_remin_in": """next reminder in """,
  "information": """info: """,
  "information_local_storage": """all your data are saved on your device !""",
};

import 'dart:async';

import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';

import 'en/jsons_en.dart';
import 'fr/jsons_fr.dart';

// #docregion Demo

class ArrayLocalizationsDelegate
    extends LocalizationsDelegate<ArrayLocalizations> {
  const ArrayLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      ArrayLocalizations.languages().contains(locale.languageCode);

  @override
  Future<ArrayLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of ArrayLocalizations.
    return SynchronousFuture<ArrayLocalizations>(ArrayLocalizations(locale));
  }

  @override
  bool shouldReload(ArrayLocalizationsDelegate old) => false;
}

class ArrayLocalizations {
  ArrayLocalizations(this.locale);

  final Locale locale;

  static ArrayLocalizations of(BuildContext context) {
    return Localizations.of<ArrayLocalizations>(context, ArrayLocalizations)!;
  }

  static const _localizedValues = <String, Map<String, String>>{
    'en': jsons_en,
    'fr': jsons_fr,
  };

  static List<String> languages() => _localizedValues.keys.toList();

  String get prayers_of_day =>
      _localizedValues[locale.languageCode]!['prayers_of_day']!;

  String get praysArray =>
      _localizedValues[locale.languageCode]!['praysArray']!;

  String get tipsArray => _localizedValues[locale.languageCode]!['tipsArray']!;
}

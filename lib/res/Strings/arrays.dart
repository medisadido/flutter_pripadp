import '../../res/Strings/fr/arrays_fr.dart';

class ArraysLocalizations {
  static const _localizedValues = <String, Map<String, dynamic>>{
    'fr': arrays_fr,
  };

  static List<String> languages() => _localizedValues.keys.toList();

  static List<String> get family_link_array => arrays_fr['family_link_array']!;
}

// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';

import 'en/strings_en.dart';
import 'fr/strings_fr.dart';

// #docregion Demo

class StringLocalizationsDelegate
    extends LocalizationsDelegate<StringLocalizations> {
  const StringLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      StringLocalizations.languages().contains(locale.languageCode);

  @override
  Future<StringLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<StringLocalizations>(StringLocalizations(locale));
  }

  @override
  bool shouldReload(StringLocalizationsDelegate old) => false;
}

class StringLocalizations {
  StringLocalizations(this.locale);

  final Locale locale;

  static StringLocalizations of(BuildContext context) {
    return Localizations.of<StringLocalizations>(context, StringLocalizations)!;
  }

  static const _localizedValues = <String, Map<String, String>>{
    'en': strings_en,
    'fr': strings_fr,
  };

  static List<String> languages() => _localizedValues.keys.toList();

  String get app_name => _localizedValues[locale.languageCode]!['app_name']!;

  String get menu_intro =>
      _localizedValues[locale.languageCode]!['menu_intro']!;

  String get menu_days => _localizedValues[locale.languageCode]!['menu_days']!;

  String get menu_tips => _localizedValues[locale.languageCode]!['menu_tips']!;

  String get menu_prays =>
      _localizedValues[locale.languageCode]!['menu_prays']!;

  String get menu_setting =>
      _localizedValues[locale.languageCode]!['menu_setting']!;

  String get menu_about =>
      _localizedValues[locale.languageCode]!['menu_about']!;

  String get navigation_drawer_open =>
      _localizedValues[locale.languageCode]!['navigation_drawer_open']!;

  String get navigation_drawer_close =>
      _localizedValues[locale.languageCode]!['navigation_drawer_close']!;

  String get intro_title =>
      _localizedValues[locale.languageCode]!['intro_title']!;

  String get intro_subtitle =>
      _localizedValues[locale.languageCode]!['intro_subtitle']!;

  String get intro_text =>
      _localizedValues[locale.languageCode]!['intro_text']!;

  String get intro_cus1 =>
      _localizedValues[locale.languageCode]!['intro_cus1']!;

  String get intro_cus2 =>
      _localizedValues[locale.languageCode]!['intro_cus2']!;

  String get intro_cus3 =>
      _localizedValues[locale.languageCode]!['intro_cus3']!;

  String get intro_cus4 =>
      _localizedValues[locale.languageCode]!['intro_cus4']!;

  String get intro_cus5 =>
      _localizedValues[locale.languageCode]!['intro_cus5']!;

  String get pray_moments =>
      _localizedValues[locale.languageCode]!['pray_moments']!;

  String get title_activity_scrolling =>
      _localizedValues[locale.languageCode]!['title_activity_scrolling']!;

  String get action_settings =>
      _localizedValues[locale.languageCode]!['action_settings']!;

  String get subtitle => _localizedValues[locale.languageCode]!['subtitle']!;

  String get tips_content_1 =>
      _localizedValues[locale.languageCode]!['tips_content_1']!;

  String get tips_content_title =>
      _localizedValues[locale.languageCode]!['tips_content_title']!;

  String get pray_pray_1 =>
      _localizedValues[locale.languageCode]!['pray_pray_1']!;

  String get pray_pray_2 =>
      _localizedValues[locale.languageCode]!['pray_pray_2']!;

  String get pray_pray_3 =>
      _localizedValues[locale.languageCode]!['pray_pray_3']!;

  String get pray_pray_4 =>
      _localizedValues[locale.languageCode]!['pray_pray_4']!;

  String get pray_pray_5 =>
      _localizedValues[locale.languageCode]!['pray_pray_5']!;

  String get pray_pray_6 =>
      _localizedValues[locale.languageCode]!['pray_pray_6']!;

  String get pray_pray_7 =>
      _localizedValues[locale.languageCode]!['pray_pray_7']!;

  String get pray_pray_8 =>
      _localizedValues[locale.languageCode]!['pray_pray_8']!;

  String get title_activity_settings =>
      _localizedValues[locale.languageCode]!['title_activity_settings']!;

  String get setting_mode_title =>
      _localizedValues[locale.languageCode]!['setting_mode_title']!;

  String get setting_police_title =>
      _localizedValues[locale.languageCode]!['setting_police_title']!;

  String get setting_apply_button =>
      _localizedValues[locale.languageCode]!['setting_apply_button']!;

  String get tips_cont_title2 =>
      _localizedValues[locale.languageCode]!['tips_cont_title2']!;

  String get tips_content_2 =>
      _localizedValues[locale.languageCode]!['tips_content_2']!;

  String get about_content =>
      _localizedValues[locale.languageCode]!['about_content']!;

  String get header_nav_img_content_description => _localizedValues[
      locale.languageCode]!['header_nav_img_content_description']!;

  String get setting_language_title =>
      _localizedValues[locale.languageCode]!['setting_language_title']!;

  String get intro_imgview_content_description => _localizedValues[
      locale.languageCode]!['intro_imgview_content_description']!;

  String get sharing_app_text_intent_title =>
      _localizedValues[locale.languageCode]!['sharing_app_text_intent_title']!;

  String get menu_dear => _localizedValues[locale.languageCode]!['menu_dear']!;

  String get name => _localizedValues[locale.languageCode]!['name']!;

  String get link => _localizedValues[locale.languageCode]!['link']!;

  String get no_saved_dear =>
      _localizedValues[locale.languageCode]!['no_saved_dear']!;

  String get suppress => _localizedValues[locale.languageCode]!['suppress']!;

  String get add_favorite =>
      _localizedValues[locale.languageCode]!['add_favorite']!;

  String get confirm_suppress =>
      _localizedValues[locale.languageCode]!['confirm_suppress']!;

  String get click_on_below_button =>
      _localizedValues[locale.languageCode]!['click_on_below_button']!;

  String get cancel => _localizedValues[locale.languageCode]!['cancel']!;

  String get add => _localizedValues[locale.languageCode]!['add']!;

  String get modify => _localizedValues[locale.languageCode]!['modify']!;

  String get veerify_maj =>
      _localizedValues[locale.languageCode]!['veerify_maj']!;

  String get alarm_off => _localizedValues[locale.languageCode]!['alarm_off']!;

  String get notification_title =>
      _localizedValues[locale.languageCode]!['notification_title']!;

  String get notification_description =>
      _localizedValues[locale.languageCode]!['notification_description']!;

  String get preference_category_date =>
      _localizedValues[locale.languageCode]!['preference_category_date']!;

  String get pick_time => _localizedValues[locale.languageCode]!['pick_time']!;

  String get cancel_reminder =>
      _localizedValues[locale.languageCode]!['cancel_reminder']!;

  String get next_remin_in =>
      _localizedValues[locale.languageCode]!['next_remin_in']!;

  String get information =>
      _localizedValues[locale.languageCode]!['information']!;

  String get information_local_storage =>
      _localizedValues[locale.languageCode]!['information_local_storage']!;

  String get author_name =>
      _localizedValues[locale.languageCode]!['author_name']!;
}
